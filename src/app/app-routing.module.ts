
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './admin/layout/login/login.component';
import { AuthGuard } from './shared/services/auth.guard';

const routes: Routes = [
  { path: 'ws-condor', component: LoginComponent },
  {
    path: 'admin',
    loadChildren: () => import('./admin/layout/painel/painel.module').then(mod => mod.PainelModule)
  }
];

//    canActivate: [AuthGuard]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
