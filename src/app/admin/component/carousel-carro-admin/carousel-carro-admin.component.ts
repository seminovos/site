import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api.service';


@Component({
  selector: 'app-carousel-carro-admin',
  templateUrl: './carousel-carro-admin.component.html',
  styleUrls: ['./carousel-carro-admin.component.scss']
})
export class CarouselCarroAdminComponent implements OnInit {

  @Input() Idcarro: number;
  @Output() img = new EventEmitter();
  flag: number;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    console.log(this.Idcarro);
    this.getUrl(this.Idcarro);
  }

  getUrl(id: any) {
    this.api.getFind('images', 'carroId', id).subscribe(data => {
      console.log(data);
      this.flag = data.length;
      if (this.flag > 0) {
        this.img = data[0].url;
        // console.log('#### IMG', this.img);
      } else {
        // console.log('#### Sin Imagem');
      }
    });
  }

}
