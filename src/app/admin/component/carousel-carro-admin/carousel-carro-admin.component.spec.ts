import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselCarroAdminComponent } from './carousel-carro-admin.component';

describe('CarouselCarroAdminComponent', () => {
  let component: CarouselCarroAdminComponent;
  let fixture: ComponentFixture<CarouselCarroAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselCarroAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselCarroAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
