import { Component, OnInit } from '@angular/core';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  closer: boolean;
  closerC: boolean;
  publish: boolean;
  preview: string;
  textPreview: string;
  tittle: string;
  description: string;
  public Editor = ClassicEditor;
  myOptions: INgxMyDpOptions = { dateFormat: 'dd.mm.yyyy' };
  model: any = { date: { year: 2018, month: 10, day: 9 } };

  constructor() {
    this.preview = 'off';
    this.textPreview = 'Não';
    this.closer = false;
    this.closerC = false;
  }

  ngOnInit() {
  }

  publicar() {
    this.publish = !this.publish;
  }

  close() { this.closer = !this.closer; }
  closeC() { this.closerC = !this.closerC; }

  isPreview(event: any) {
    if (this.preview === 'off') {
      this.textPreview = 'Sim';
      this.preview = 'on';
    } else {
      this.textPreview = 'Não';
      this.preview = 'off';
    }
  }
  public onChange( { editor }: ChangeEvent ) {
    this.description = editor.getData();
  }
}
