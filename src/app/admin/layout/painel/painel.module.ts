import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { PainelRoutingModule } from './painel-routing.module';
import { PainelComponent } from './painel.component';
import { NavigationComponent } from '../../helper/navigation/navigation.component';
import { BlogComponent } from '../blog/blog.component';
import { AddComponent } from '../blog/add/add.component';
import { AuthService } from './../../../shared/services/auth.service';
import { AuthGuard } from './../../../shared/services/auth.guard';


@NgModule({
  declarations: [
    PainelComponent,
    NavigationComponent,
    BlogComponent,
    AddComponent
  ],
  imports: [
    CommonModule,
    PainelRoutingModule,
    CKEditorModule,
    NgxMyDatePickerModule.forRoot(),
    MDBBootstrapModule.forRoot()
  ],
  providers: [AuthGuard, AuthService],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})
export class PainelModule { }
