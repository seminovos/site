
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PainelComponent } from './painel.component';
import { BlogComponent } from '../blog/blog.component';
import { AddComponent } from '../blog/add/add.component';

const routes: Routes = [
{
  path: '',
  component: PainelComponent,
  children: [
    { path: 'blog', component: BlogComponent },
    { path: 'blog/add', component: AddComponent },
    { path: 'veiculo',  loadChildren:  () => import('../veiculo/veiculo.module').then(mod => mod.VeiculoModule) },
    { path: 'services', loadChildren: () => import('../tabelas/tabelas.module').then(mod => mod.TabelasModule) },
    { path: 'leads', loadChildren: () => import('../leads/leads.module').then(mod => mod.LeadsModule) }
  ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PainelRoutingModule { }
