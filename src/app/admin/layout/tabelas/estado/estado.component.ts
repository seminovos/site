import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataApiService } from 'src/app/shared/services/data-api.service';


@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.scss']
})
export class EstadoComponent implements OnInit {

  fmEstado: FormGroup;
  estado: any;

  constructor(
    private fb: FormBuilder,
    private api: DataApiService
  ) { }

  ngOnInit() {
    this.loadForm()
    this.api.getAll('estados').subscribe(data => this.estado = data);
  }

  private loadForm() {
    this.fmEstado = this.fb.group({
      nome: ['', Validators.required],
      uf: ['', Validators.required]
    });
  }

  onsubmit() {
    if (!this.fmEstado.valid) { return; }
    this.api.postAdd('estados', this.fmEstado.value)
    .subscribe(data => {
      console.log(data);
    });
  }

  onEdit() {

  }

}
