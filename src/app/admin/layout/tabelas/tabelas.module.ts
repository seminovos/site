import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { TabelasRoutingModule } from './tabelas-routing.module';
import { EstadoComponent } from './estado/estado.component';


@NgModule({
  declarations: [EstadoComponent],
  imports: [
    CommonModule,
    TabelasRoutingModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot()
  ]
})
export class TabelasModule { }
