import { Component, OnInit,  ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api.service';
import { MdbTableDirective, MdbTablePaginationComponent } from 'angular-bootstrap-md';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})
export class LeadsComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  elements: any = [];
  previous: any = [];
  headElements = ['ID', 'Nome', 'Email', 'Telefone', 'Veículo'];
  leads: any;

  constructor(private api: DataApiService, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getLeads();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  getLeads() {
    this.api.getAll('leads').subscribe(data => this.leads = data);
  }

}
