import { RouterModule } from '@angular/router';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VeiculoRoutingModule } from './veiculo-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { VeiculoComponent } from './veiculo.component';
import { CarouselCarroAdminComponent } from '../../component/carousel-carro-admin/carousel-carro-admin.component';
import { AddComponent } from './add/add.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ImageUploadModule } from 'angular2-image-upload';
import { DetalhesComponent } from './detalhes/detalhes.component';

@NgModule({
  declarations: [
    VeiculoComponent,
    CarouselCarroAdminComponent,
    AddComponent,
    DetalhesComponent
  ],
  imports: [
    CommonModule,
    VeiculoRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    ImageUploadModule.forRoot(),
    MDBBootstrapModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VeiculoModule { }
