import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api.service';

@Component({
  selector: 'app-veiculo',
  templateUrl: './veiculo.component.html',
  styleUrls: ['./veiculo.component.scss']
})
export class VeiculoComponent implements OnInit {

  veiculo: any;
  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.getVeiculo();
  }

  private getVeiculo() {
    const collection = 'filter[order]id%20DESC&filter[limit]=11';
    this.api.getAll('carros', collection).subscribe(data => {
      const sortedInput = data.slice(0).sort((a, b) => b.id - a.id);
      this.veiculo = sortedInput;
    });
  }

  apagar(id: number) {
    console.log(id);
  }
}
