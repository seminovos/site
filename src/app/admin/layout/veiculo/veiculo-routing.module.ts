import { DetalhesComponent } from './detalhes/detalhes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VeiculoComponent } from './veiculo.component';
import { AddComponent } from './add/add.component';

const routes: Routes = [
  { path: '', component: VeiculoComponent },
  { path: 'add', component: AddComponent },
  { path: 'preview/:id', component: DetalhesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculoRoutingModule { }
