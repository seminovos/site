import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileHolder } from 'angular2-image-upload';
import { ImagemService } from './../../../../shared/services/imagem.service';
import { DataApiService } from './../../../../shared/services/data-api.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  flag: boolean;
  estado: any;
  cidade: any;
  marca: any;
  opc: any;
  oferta: any;
  img1: any;
  opcionais: any;
  fmVeiculo: FormGroup;
  fmOption: FormGroup;
  fmComplem: FormGroup;
  images: any;
  id: number;


  constructor(
    private fb: FormBuilder,
    private api: DataApiService,
    private apiImage: ImagemService
  ) { }

  ngOnInit() {
    this.loadForm();
    this.getEstado();
    this.getOpcionais();
    this.getOfertas();
    this.getMarca();

  }

  private loadForm() {
    this.fmVeiculo = this.fb.group({
      modeloCarro: ['', Validators.required],
      marcaCarro: ['', Validators.required],
      cor: ['', Validators.required],
      ano_fabricacao: ['', Validators.required],
      ano_modelo: ['', Validators.required],
      kilometragem: ['', Validators.required],
      combustivel: ['', Validators.required],
      portas: ['', Validators.required],
      transmissao: ['', Validators.required],
      estado: ['', Validators.required],
      cidade: ['', Validators.required],
      ofertas: ['', Validators.required],
      status: ['disponivel'],
      valor: ['', Validators.required],
      placa: ['', Validators.required],
      versao: ['', Validators.required],
      descricao: ['', Validators.required]

    });

    this.fmOption = this.fb.group({
      arCondicionado: [''],
      arBag: [''],
      travasEletricas: [''],
      direcaoHidraulica: [''],
      direcaoEletrica: [''],
      brakeLight: [''],
      vidrosEletricos: [''],
      limpadorTraseiro: [''],
      freioABS: [''],
      radio: [''],
      dvdPlayer: [''],
      vidrosTraseirosEletricos: [''],
      espelhosRetrovisoresTraseiros: [''],
      bancosEnCouro: [''],
      altosFalantes: [''],
      gps: [''],
      alarme: [''],
      protetorDeCacamba: [''],
      sensorEstaciomamentoTraseiro: [''],
      carroId: ['']
    });

    this.fmComplem = this.fb.group({
      baixa_km: [''],
      feito_cautelar: [''],
      garantia_loja: [''],
      garantia_fabrica: [''],
      ipva_pago: [''],
      licenciado: [''],
      nenhuma_batida: [''],
      pneus_novos: [''],
      revisoes_realizadas: [''],
      unico_dono: [''],
      carroId: ['']
    });

  }

  getEstado() { this.api.getAll('estados').subscribe(m => this.estado = m); }
  getOfertas() {  this.api.getAll('ofertas').subscribe(m => this.oferta = m); }
  getOpcionais() { this.api.getAll('opcionais').subscribe(m =>  this.opc = m); }
  getMarca() { this.api.getAll('marcas').subscribe(m => this.marca = m); }
  onGetCidade(event) { this.api.getFind('cidades', 'estado', event.target.value).subscribe(m =>  this.cidade = m); }

  /** Submit */
  onSubmit() {
    if (!this.fmVeiculo.valid) { return; }
    this.api.postAdd('carros', this.fmVeiculo.value)
    .subscribe(data => {
      
    });
  }

  /** Images */
  onUploadFinished(file: FileHolder) {
    const form = {
      url: `https://api.autoparvioutlet.com.br/api/storages/photos/download/${file.file.name}`,
      carroId: this.id,
      nome: file.file.name
    };
    this.api.postAdd('images', form).subscribe(data => data);
  }

  onRemoved(file: FileHolder) {
    this.apiImage.delImage('photos', file.file.name).subscribe(data => data);
  }

  onUploadStateChanged(state: boolean) {
    console.log(state);
  }

}
