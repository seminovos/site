import { DataApiService } from 'src/app/shared/services/data-api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.scss']
})
export class DetalhesComponent implements OnInit {

  id: number;
  carro: any;
  estado: string;
  cidade: string;
  oferta: string;
  imagen: any;
  opcionais: any;
  complemento: any;
  people: any;

  constructor(
    private activate: ActivatedRoute,
    private router: Router,
    private api: DataApiService
  ) {
    this.activate.url.subscribe(u => {
      this.id = this.activate.snapshot.params.id;
      this.onExits(this.id);
    });
  }

  ngOnInit() {
  }

  onExits(id: any) {
    this.api.getFind('carros', 'id', id)
    .subscribe(data => {
      this.carro = data;
      this.getEstado(data[0].estado);
      this.getCidade(data[0].cidade);
      this.getOferta(data[0].ofertas);
      this.getImages(id);
      this.getOpcionais(id);
      this.getComplemento(id);
    });
  }

  /** Campus Adicionais */
  getEstado(id: any) {
    this.api.getFind('estados', 'id', id)
    .subscribe(data => this.estado = data[0].nome);
  }

  getCidade(id: any) {
    this.api.getFind('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0].nome);
  }

  getOferta(id: any) {
    this.api.getFind('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0].nome);
  }

  getImages(id: any) {
    this.api.getFind('images', 'carroId', id)
    .subscribe(data => this.imagen = data);
  }

  getOpcionais(id: any) {
    this.api.getFind('opcionais', 'carroId', id)
    .subscribe(data => this.opcionais = data);
  }

  getComplemento(id: any) {
    this.api.getFind('complementos', 'carroId', id)
    .subscribe(data => this.complemento = data);
  }

}
