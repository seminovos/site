import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataApiService } from 'src/app/shared/services/data-api.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @ViewChild('sidenav', {static: true}) sidenav: ElementRef;

  clicked: boolean;
  visible: boolean;
  menu: any[];
  constructor(private api: DataApiService) {
    this.visible = this.visible === undefined ? true : false;
    this.clicked = this.clicked === undefined ? false : true;
  }

  ngOnInit() {

  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }

  getToogle() {
    this.visible = !this.visible;
    return this.visible;
  }
}
