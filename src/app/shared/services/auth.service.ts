import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string     = environment.api.url;
  token: string = environment.api.token;

  constructor(private http: HttpClient) { }

  /**
   * LocalStorage
   */
  isLoggedIn() {
    return localStorage.getItem('access') ? true : false;
  }

  getAccessToken() {
    return JSON.parse(localStorage.getItem('access')).id;
  }

  getAccessTokenUserId() {
    return JSON.parse(localStorage.getItem('access')).userId;
  }

  /**
   * Sign-In
   */
  signIn(data: any) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json; charset=utf-8'});
    return this.http.post(`${this.url}/Users/login`, data, {headers});
  }

  /**
   * Sign-Out
   */
  signOut() {
    const accessToken = this.getAccessToken();
    return this.http.post(`${this.url}/Users/logout?access_token=${accessToken}`, {});
  }

  /**
   * getUser
   */
  getUser() {
    const accessToken = this.getAccessToken();
    const userId = this.getAccessTokenUserId();
    return this.http.get(`${this.url}/Users/${userId}?access_token=${accessToken}`);
  }

  getUserCount() {
    const accessToken = this.getAccessToken();
    const headers = new HttpHeaders({'Access-Control-Allow-Origin' : '*'});
    return this.http.get(`${this.url}/Users/count?access_token=${accessToken}`, {headers});
  }

}
