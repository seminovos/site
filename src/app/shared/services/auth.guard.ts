import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate() {
    // Verifica se existe Token
    if (this.authService.isLoggedIn()) {
      // Caso exista token retorna true
      return true;
    } else {
      // Caso não exista envia para página de cpf não é cadastrado
      this.router.navigate(['']);
      return false;
    }
  }
}
