import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ImagemService {

  url: string     = environment.api.url;
  token: string = environment.api.token;
  headers = new HttpHeaders({
    'Content-Type' : 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*',
  });

  constructor(private http: HttpClient) { }

  upImage(collection: string, data: any) {
    return this.http.post(`${this.url}/storages/${collection}/upload`, data, {headers: this.headers});
  }

  delImage(collection: string, nome: any) {
    return this.http.delete(`${this.url}/storages/${collection}/files/${nome}`, {headers: this.headers});
  }

}
