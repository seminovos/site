import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  url: string     = environment.api.url;
  token: string = environment.api.token;

  constructor(private http: HttpClient) { }

  /**
   * getTable
   * @param table
   */
  getAll(table: string, collection: string = ''): Observable<any[]> {
    console.log(`${this.url}/${table}?${collection}`);
    return this.http.get<any[]>(`${this.url}/${table}?${collection}`).pipe( tap(data => data) );
  }

  getId(table: string, id: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${table}/${id}`).pipe( tap(data => data) );
  }

  getCount(table: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${table}/count`).pipe( tap(data => data) );
  }

  getFind(table: string, field: string, search: string, collection: string = ''): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${table}?filter[where][${field}]=${search}&${collection}`).pipe( tap(data => data) );
  }

  getCountFind(table: string, field: string, search: string, collection: string = ''): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${table}/count?[where][${field}]=${search}&${collection}`).pipe( tap(data => data) );
  }

  /**
   * Add registro
   * @param table
   * @param data
   */
  postAdd(table: string, data: any): Observable<any[]> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json; charset=utf-8'});
    return this.http.post<any[]>(`${this.url}/${table}`, data, {headers});
  }

  /**
   *
   * @param table Edit pro ID
   * @param id
   * @param data
   */
  putEdit(table: string, id: string, data: any): Observable<any[]> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json; charset=utf-8'});
    return this.http.put<any[]>(`${this.url}/${table}/${id}`, data, {headers});
  }

  /**
   * Delete pro ID
   * @param table
   * @param id
   */
  delApagar(table: string, id: string): Observable<any[]> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json; charset=utf-8'});
    return this.http.delete<any[]>(`${this.url}/${table}/${id}`, {headers});
  }
}
